
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const _ = require('lodash')
const webpackMerge = require('webpack-merge')
const {print} = require('q-i');
// Wait until webpack-blocks-happypack is upgraded and ready for webpack4
// const happypack = require('webpack-blocks-happypack')

const webpack = require('webpack')
const {
  createConfig,
  match,
  merge,
  group,

  // Feature blocks
  css,
  devServer,
  defineConstants,
  file,
  postcss,
  uglify,

  // Shorthand setters
  addPlugins,
  setEnv,
  entryPoint,
  env,
  setOutput,
  sourceMaps
} = require('webpack-blocks')

function addLoader (loaderDef) {
  const cleanedLoaderDef = _.omitBy(loaderDef, _.isUndefined)
  return (context, util) => prevConfig => webpackMerge.smart(prevConfig, {
    module: {
      rules: [ cleanedLoaderDef ]
    }
  });
};

function addLoaders (loaders) {
  return group(loaders.map((loader)=>{return addLoader(loader);}));
};
const autoprefixer = require('autoprefixer')
const HappyPack = require('happypack')
const happyThreadPool = HappyPack.ThreadPool({size: 8});
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000
const sourceDir = process.env.SOURCE || 'src'
const publicPath = `/${process.env.PUBLIC_PATH || ''}/`.replace('//', '/')
const sourcePath = path.resolve(process.cwd(), sourceDir)
const outputPath = path.join(process.cwd(), 'dist')
const styleSource = path.resolve(__dirname,sourceDir+'/styles/styles.scss')

const splitvendor = (vendors) => {
  return (context, util) => prevConfig => {
    let curPlugs = prevConfig.plugins? prevConfig.plugins:[];
    return {
      ...prevConfig,
      entry: {
        ...prevConfig.entry,
        vendors: prevConfig.vendors.concat(vendors),
      },
      plugins: prevConfig.plugins.concat([
        new webpack.optimize.CommonsChunkPlugin({name: "vendors", filename: "vendors.bundle.js"}),
      ])
    };
  }
}


const happypack = () => group([
  addLoader({ test: /\.js?$/, exclude: /^(node_modules\/?!(\breact-human-timer\b|\bhuman-timer\b|\bstyled-component\b)).*$/, loader: 'happypack/loader?id=js' }),
  addLoader({ test: /\.pug?$/, exclude: /node_modules/, loader: 'happypack/loader?id=pug' }),
  addPlugins([
    new HappyPack({
      id: 'js',
      threadPool: happyThreadPool,
      loaders: ['babel-loader']
    }),
    new HappyPack({
      id: 'pug',
      threadPool: happyThreadPool,
      loaders: ['babel-loader','pug-as-jsx-loader']
    }),
  ]),
]);

const babel = () => group([
  addLoader({ test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' }),
  addLoader({ test: /\.pug?$/, exclude: /node_modules/, loaders: ['babel-loader','pug-as-jsx-loader']}),
]);


const sassLoaders = [
  {
    loader: 'css-loader',
    options: {
/*       camelCase: true,
      modules: true, */
      modules: false,
      sourceMap: true,
      importLoaders: 3
    }
  },
  {
    loader: 'postcss-loader',
    options:{
      sourceMap: true,
      plugins: [
        autoprefixer
      ],
      options: {
        parser: 'sugarss',
        autoprefixer: true
      },
    }
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      outputStyle: 'expanded',
      includePaths: [path.resolve(__dirname,'node_modules')]
    }
  },
];

const assets = () => group([
  addPlugins([
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]),
  addLoader({ test: /\.(png|jpe?g|svg|woff2?|ttf|eot)$/, loader: 'url-loader?limit=8000' }),
  addLoader({ test: /\.(s(c|a)ss)$/,
    exclude: '/node_modules/',
    use: [MiniCssExtractPlugin.loader].concat(sassLoaders)
  }),
]);

const storybookAssets = () => group([
  addLoader({ test: /\.(png|jpe?g|svg|woff2?|ttf|eot)$/, loader: 'url-loader?limit=8000' }),
  addLoader({ test: /\.s(a|c)ss$/,
    exclude:  '/(\.module\.(scss|sass)$)/',
    use:  sassLoaders,
  }),
]);


const resolveModules = (modules) => {
  return (context, util) => prevConfig => {
    curModules = prevConfig.resolve.modules? prevConfig.resolve.modules:[];
    return {
      ...prevConfig,
      resolve: {
        modules: curModules.concat([modules,'node_modules']),
      }
    }
  }
}

const base = () => group([
  entryPoint({
    app: sourcePath,
    styles: styleSource
  }),
  setOutput({
    filename: '[name].js',
    path: outputPath,
    publicPath,
  }),
  defineConstants({
    'process.env.NODE_ENV': process.env.NODE_ENV,
    'process.env.PUBLIC_PATH': publicPath.replace(/\/$/, ''),
  }),
  //happypack(),
  //babel(),
  // happypack([
  //   babel(),
  // ]),
  resolveModules(sourcePath),

  env('development', [
    devServer({
      contentBase: 'public',
      stats: 'errors-only',
      historyApiFallback: { index: publicPath },
      headers: { 'Access-Control-Allow-Origin': '*' },
      proxy: {
        changeOrigin: true,
        '/api': {
          target: 'https://stoutshout-backend.vmlaustralia.com/',
          secure: false,
          changeOrigin: true,
          pathRewrite: {"^/api":""}
        }
      },
      host,
      port,
    }),
    sourceMaps(),
    addPlugins([
      new webpack.NamedModulesPlugin(),
    ]),
  ]),

  env('production', [
  ]),
]);

const config = createConfig([
  base(),
  addPlugins([
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.join(process.cwd(), 'public/index.html'),
    }),
  ]),
  happypack(),
  assets(),
])

const storybook = createConfig([
  base(),
  happypack(),
  storybookAssets(),
]);

module.exports = {default: config, storybook: storybook}
