# Install
```
	npm i
```
## Get into dev work

```
	npm run dev
```

## Storybook
```
	npm run storybook
```

## Jest Tests
```
	npm run tests
```

##Crunch the CSV to JSON (CLI)
```
jq -Rsnc --raw-output '
    [inputs
     | . / "\r\n"
     | (.[] | select(length > 0) | . / ",") as $input
     | {"label": $input[0], "location": {"lat":$input[1], "lng":$input[2]}}]
' <Feb_stout_shout.csv
```
