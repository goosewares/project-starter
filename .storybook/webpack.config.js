const parentConfig = require('../webpack.config');
const webpackMerge = require('webpack-merge');
const R = require('ramda');
const { print } = require('q-i');

module.exports = (baseConfig,env, defaultConfig) =>{
  //console.log('default config');
  //print(baseConfig)
  baseConfig.module.rules = R.reject(o=>{console.log(o.test.toString(),'/\\.(scss|sass)$/',o.test.toString() == "/\\.(scss|sass)$/",R.has('test',o)); return (R.has('test',o) && o.test.toString() == '/\\.(scss|sass)$/')},baseConfig.module.rules);
  //print(baseConfig)
  //console.log('storybook config')
  parentConfig.storybook.plugins = R.reject(o=>{return o.constructor.name.match(/(HotModuleReplacementPlugin|NamedModulesPlugin)/ig);},parentConfig.storybook.plugins);
  //print(parentConfig.storybook.plugins);  
  let configObj = webpackMerge.smart(baseConfig, {
    /**/entry:  {
        preview: ['babel-polyfill'],
        //app: parentConfig.storybook.entry.app,
        styles: parentConfig.storybook.entry.styles
    },/**/
    resolve:{ 
        modules: parentConfig.storybook.resolve.modules,
    },
    module: { 
      rules: parentConfig.storybook.module.rules.slice(1),
    },/**/
    plugins: parentConfig.storybook.plugins,
    /**/
  });

  //console.log('Full config');
  //print(configObj);

 return configObj;
  return baseConfig;
}
