
import React from 'react';
import { configure, addDecorator, setAddon } from '@storybook/react';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import configureStore from 'store/configure'
import chaptersAddon from 'react-storybook-addon-chapters';
import theme from '../src/components/themes/default';
import css from '../src/styles/styles.scss';
import LiveEdit, {setOptions} from 'storybook-addon-react-live-edit';
//import api from 'services/api'

const store = configureStore({}, /*{ api: api.create() }*/)
const req = require.context('../src/components', true, /.stories.js$/)

// Chapters
setAddon(chaptersAddon)

// Live Edit
setOptions({ theme: 'darcula', presets: ['react'] });
setAddon(LiveEdit);

function loadStories() {
  req.keys().forEach(filename => {
      return req(filename);
    }
  )
}

addDecorator(story => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <div>
            <style>{css.toString()}</style>
            {story()}
          </div>
        </ThemeProvider>
      </BrowserRouter>
    </Provider>
  )
});

configure(loadStories, module)
