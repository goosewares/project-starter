import React from 'react'
import Ssuggestor from 'Ssuggestor'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import {prop} from 'styled-tools'

const AutoSuggestor = styled.div`
  .geosuggest__suggests-wrapper {
    position: relative;
  }
  .geosuggest__input {
    width: 100%;
    background-color: transparent;
    border: none;
    border: 1px solid ${palette('primary',2)};
    padding: .5em 1em .5em 30px;
    font-weight: 600;
    color: white;
  }
  .geosuggest{
    position:relative;
    :before {
      content: '';
      display: inline-block;
      background-image: url(${prop('image')});
      background-repeat: no-repeat;
      background-size: contain;
      background-position: center;
      width: 16px;
      height: 30px;
      position: absolute;
      left: 8px;
      top: 3px;
    }
  }
  .geosuggest__input:focus {
    border-color: ${palette('primary',2)};
    box-shadow: 0 0 0 transparent;
  }
  .geosuggest__item--active{
    font-weight: 600;
    &.geosuggest__item strong{
        font-weight: 600;
    }
  }
  .geosuggest__item {
      a{
          color: white;
          font-size: 15px;
      }
      strong{
        font-weight:normal;
      }
  }
  .geosuggest__suggests {
    position: relative;
    top: 100%;
    left: 0;
    right: 0;
    margin: -17px 0;
    max-height: 25em;
    padding: 0 0 10px;
    background: #000;
    color: white;
    border: 1px solid ${palette('primary',2)};
    border-top-width: 0;
    overflow-x: hidden;
    overflow-y: auto;
    list-style: none;
    z-index: 5;
    -webkit-transition: max-height 0.2s, border 0.2s;
            transition: max-height 0.2s, border 0.2s;
  }
  .geosuggest__suggests--hidden {
    max-height: 0;
    overflow: hidden;
    border-width: 0;
  }
  li.geosuggest__item {
    text-align: left;
    margin: 0 10px;
  }
`

export default (props)=>{
    const {list,placeholder,onSelect,openOnClick,selector,image} = props;
    const autoProps = {
        theme: {
            root: 'geosuggest',
            input: 'geosuggest__input',
            arrow: 'geosuggest__arrow',
            close: 'geosuggest__remove',
            list: 'geosuggest__suggests',
            item: 'geosuggest__item',
            activeItem: 'geosuggest__item--active'
        },
        list: list,
        openOnClick: openOnClick,
        placeholder: placeholder,
        onSelect: onSelect,
        selector: typeof selector != 'undefined'? selector:({label})=>label
    }
    return (
        <AutoSuggestor image={image}>
            <Ssuggestor {...autoProps}/>
        </AutoSuggestor>
    )
}
