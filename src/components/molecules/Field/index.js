import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Label, Input, Block } from 'components'
import { font, palette } from 'styled-theme'

const Error = styled(Block)`
  margin: 0.5rem 0 0;
  text-align: left;
`

const Wrapper = styled.div`
  margin-bottom: 1rem;
  input[type="checkbox"],
  input[type="radio"] {
    margin-right: 0.5rem;
  }
  label {
    vertical-align: middle;
    color: ${palette('primary',0)};
    text-align: left;
  }
`

const Field = ({
  error, name, invalid, label, type, ...props
}) => {
  const inputProps = {
    id: name, name, type, invalid, 'aria-describedby': `${name}Error`, ...props,
  }
  const renderInputFirst = type === 'checkbox' || type === 'radio'
  return (
    <Wrapper>
      {label && <Label htmlFor={inputProps.id}>
        {renderInputFirst && <Input {...inputProps} />}
        {label}
        {renderInputFirst || <Input {...inputProps} />}
      </Label>}
      {!label && <Input {...inputProps} />}
      {invalid && error &&
        <Error id={`${name}Error`} role="alert" palette="danger">
          {error}
        </Error>
      }
    </Wrapper>
  )
}

Field.propTypes = {
  name: PropTypes.string.isRequired,
  invalid: PropTypes.bool,
  error: PropTypes.string,
  type: PropTypes.string,
}

Field.defaultProps = {
  type: 'text',
}

export default Field
