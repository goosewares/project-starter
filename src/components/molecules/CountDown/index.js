import React from 'react'
import HumanTimer from 'react-human-timer'
import Content from './content.pug'
import moment from 'moment'

export default (props)=>{
    const {onEnd,toDate,className, activate} = props;
    const timer = {
        minutes: '15',
        seconds: '00'
    }
    let m = moment,
        seconds = 15*60;
    if(typeof toDate != 'undefined'){
        seconds = toDate.diff(moment(),'Seconds');
        seconds = seconds > 0 ? seconds:0;
    }else{
        seconds = 15*60;
    }

    if(typeof props.toDate == 'undefined'){
        return (<Content classes={className} {...timer} />)
    }
    
    return (
        <div>
            <HumanTimer
                onEnd = {onEnd}
                seconds = {seconds}
                className= {className}
            >
            {
                timer=>{
                    return (
                        <Content classes={className} {...timer} />
                    )}
            }
            </HumanTimer>
        </div>
    )
}
