import React from 'react'
import { Switch, Route } from 'react-router-dom'
import {Router} from 'components/containers'

import { createGlobalStyle, ThemeProvider } from 'styled-components'

import theme from './themes/default'

const fonts = ['/fonts/LeagueGothicRegular','/fonts/RobotoMedium','fonts/RobotoLight','fonts/RobotoCondensedLight'];

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0 20px;
    background-color: #000;
    text-align: center;
    color: #fff;
    font-family: ${theme.fonts.primary};
  }
  
  @font-face {
    font-family: "League Gothic Regular";
    src: url("${fonts[0]}/font_file.eot"); /* IE9*/
    src: url("${fonts[0]}/font_file?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("${fonts[0]}/font_file.woff2") format("woff2"), /* chrome、firefox */
    url("${fonts[0]}/font_file.woff") format("woff"), /* chrome、firefox */
    url("${fonts[0]}/font_file.ttf") format("truetype"); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  }
  @font-face {
    font-family: "Roboto";
    font-weight: 600;
    src: url("${fonts[1]}/font_file.eot"); /* IE9*/
    src: url("${fonts[1]}/font_file?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("${fonts[1]}/font_file.woff2") format("woff2"), /* chrome、firefox */
    url("${fonts[1]}/font_file.woff") format("woff"), /* chrome、firefox */
    url("${fonts[1]}/font_file.ttf") format("truetype"); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  }
  @font-face {
    font-family: "League Gothic Regular";
    src: url("${fonts[0]}/font_file.eot"); /* IE9*/
    src: url("${fonts[0]}/font_file?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("${fonts[0]}/font_file.woff2") format("woff2"), /* chrome、firefox */
    url("${fonts[0]}/font_file.woff") format("woff"), /* chrome、firefox */
    url("${fonts[0]}/font_file.ttf") format("truetype"); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  }
  @font-face {
    font-family: "Roboto";
    src: url("${fonts[2]}/font_file.eot"); /* IE9*/
    src: url("${fonts[2]}/font_file?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("${fonts[2]}/font_file.woff2") format("woff2"), /* chrome、firefox */
    url("${fonts[2]}/font_file.woff") format("woff"), /* chrome、firefox */
    url("${fonts[2]}/font_file.ttf") format("truetype"); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  }
  @font-face {
    font-family: "RobotoCondensed";
    src: url("${fonts[3]}/font_file.eot"); /* IE9*/
    src: url("${fonts[3]}/font_file?#iefix") format("embedded-opentype"), /* IE6-IE8 */
    url("${fonts[3]}/font_file.woff2") format("woff2"), /* chrome、firefox */
    url("${fonts[3]}/font_file.woff") format("woff"), /* chrome、firefox */
    url("${fonts[3]}/font_file.ttf") format("truetype"); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  }
  h2{
    font-family: "RobotoCondensed";
    color: ${theme.palette.primary[2]};
    text-transform: uppercase;
  }
  a{
    text-transform: uppercase;
    font-size: 24px;
  }
`

const App = () => {
  return (
      <ThemeProvider theme={theme}>
        <div>
          <Router />
          <GlobalStyle />
        </div>
      </ThemeProvider>
  )
}
export default App
