import React from 'react'
import styled from 'styled-components'
import { Hamburger, Link } from 'components/atoms'
import { size, font } from 'styled-theme'
import menu from './menu.pug'

//import { IconLink, PrimaryNavigation, Block } from 'components'
const Menu = styled(menu)`
  width: 100%;
  > div {
    margin-top: 40px;
    height: calc(100vh - 40px);
    position: relative;
  }
  .hamburger-footer{
    position: absolute;
    bottom: 80px;
    left: 0;
    right: 0;
    img {
      max-width: 250px;
      margin-top: 10px;
    }
    a{
      font-family: ${font('primary')};
      font-size: 1em;
      color: white;
      text-transform: none;
    }
  }
  ul{
    list-style-type: none;
    margin-left: 0px;
    li{
      font-family: "RobotoCondensed";
      padding: 15px 15px;
      font-weight: 500;
      letter-spacing: 1px;
      font-size: 20px;
      text-transform: uppercase;
      text-align: left;
      border-top: 1px solid #252525;
    }
  }
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem;
  background-color: black;
  width: 100%;
  min-height: 40px;
  @media screen and (max-width: 640px) {
    padding: 0.5rem;
  }
  
`

const InnerWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  max-width: ${size('maxWidth')};
  > :not(:first-child) {
    margin-left: 1rem;
  }
`
const Logo = styled.span`
  height: 25px;
  width: 94px;
  position: relative;
  background-image: url('/images/header-logo.png');
  background-repeat: no-repeat;
  background-size: contain;
  position: absolute;
  left: 20px;
  top: 15px;
`

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      hamburger: false
    }
    this.hamburgerClick = this.hamburgerClick.bind(this)
  }
  hamburgerClick(){
    this.setState({hamburger:!this.state.hamburger})
  }
  render(){
    const props = this.props;
    return (
      <Wrapper opaque reverse {...props}>
        <Link to={`/`}>
          <Logo />
        </Link>
        <Hamburger HamburgerClick={this.hamburgerClick} openHamburger={this.state.hamburger} />
        <Menu Link={Link} hamburger={this.state.hamburger} />
      </Wrapper>
    )
  }
}

export default Header
