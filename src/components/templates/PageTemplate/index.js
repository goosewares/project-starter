import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { size } from 'styled-theme'
import Page from './page.pug'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 3.75rem;
  min-height: 100vh;
  box-sizing: border-box;
  @media screen and (max-width: 640px) {
    padding-top: 3.25rem;
  }
`

const Header = styled.header`
  position: fixed;
  top: 0;
  left: 0px;
  width: 100%;
  z-index: 999;
`

const Hero = styled.section``

const Sponsor = styled.section``

const Content = styled.section`
  width: 100%;
  box-sizing: border-box;
  margin: 2rem auto;
  max-width: ${size('maxWidth')};
`

const Footer = styled.footer`
  margin-top: auto;
`

const PageTemplate = ({
  header, hero, sponsor, children, footer, ...props
}) => {
  
  const components = {
    Wrapper: Wrapper,
    Header: Header,
    Hero: Hero,
    Sponsor: Sponsor,
    Content: Content,
    Footer: Footer
  }
  
  return (
    <Page { ...components } props={props} header={header} hero={hero} sponsor={sponsor} footer={footer} >
      {children}
    </Page>
  )
}

PageTemplate.propTypes = {
  header: PropTypes.node.isRequired,
  hero: PropTypes.node,
  sponsor: PropTypes.node,
  footer: PropTypes.node,
  children: PropTypes.any.isRequired,
}

export default PageTemplate
