
import { reversePalette } from 'styled-theme/composer'

const theme = {}

theme.palette = {
  primary: ['#fff', 'rgb(30, 135, 80)', '#bd955b', '#233029','#2a9b5f'],
  secondary: ['#c2185b', '#e91e63', '#000', '#5C5C54'],
  danger: ['#d32f2f', '#f44336', '#f8877f', '#ffcdd2'],
  alert: ['#ffa000', '#ffc107', '#ffd761', '#ffecb3'],
  success: ['#388e3c', '#4caf50', '#7cc47f', '#c8e6c9'],
  white: ['#fff', '#fff', '#eee'],
  grayscale: [
    '#212121',
    '#414141',
    '#616161',
    '#9e9e9e',
    '#bdbdbd',
    '#e0e0e0',
    '#eeeeee',
    '#ffffff',
  ],
}

theme.reversePalette = reversePalette(theme.palette)

theme.fonts = {
  primary: '"Roboto", Helvetica, Roboto, sans-serif',
  pre: '"League Gothic Regular", Consolas, Liberation Mono, Menlo, Courier, monospace',
  quote: 'Georgia, serif',
}

theme.sizes = {
  maxWidth: '1100px',
}

export default theme
