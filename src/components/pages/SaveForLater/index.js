import React from 'react'
import { Link, Button, PageTemplate, Header, Hero, Footer, Divider, HeroImage } from 'components'
import { Redirect } from 'react-router-dom'
import styled, { createGlobalStyle } from 'styled-components'

const Img = styled.img`
  margin-top: 180px;
  padding-bottom:8px;
  width: 266px;
  
`
const Span = styled.span`
  margin: 0 30px;
  display: block;
  font-weight: 600;
`

class SaveForLater extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      continue: false,
    }
    this.continue = this.continue.bind(this);
  }
  componentDidMount(){
    localStorage.currentStage = 'saved';
  }
  continue(){
    localStorage.currentStage = 'claim'
    this.setState({
      continue: true
    })
  }
  render(){
    if(this.state.continue){
      return(<Redirect to='/claim' />)
    }
    return (
      <PageTemplate
        header={<Header />}
        hero={<HeroImage image="/images/BG.png" />}
        //footer={<Footer />}
      >
        <Img src='images/headings/STOUT_SHOUT.svg' />
        <Divider/>
        <Span>You're successfully registered for your free pint of Guinness. To redeem, simply come back to this page on the same device and click continue to select the venue you would like to redeem at. </Span>
        <p><br/><Button onClick={this.continue}>Continue</Button></p>
      </PageTemplate>
    )
  }
}  
  export default SaveForLater
  