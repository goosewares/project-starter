
import React from 'react'
import Content from './content.pug'
import ContentWrong from './contentWrongAnswer.pug'
import { Redirect } from 'react-router-dom'
import { Link, Button, PageTemplate, Header, Hero, Footer, Field as FormField, Divider, HeadingImage, Icon } from 'components'
import styled from 'styled-components'

const Contents = styled(Content)`
  label {
    width: 271px;
    margin: 0 auto;
  }
`;
let RenderContent = Contents

class Questions extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      selectedValue: '',
      firstQuestion: true,
      secondQuestion: false,
      success: false
    }
    this.updateInputValue = this.updateInputValue.bind(this);
    this.checkQuestion = this.checkQuestion.bind(this);
    this.tryAgain = this.tryAgain.bind(this);
  }
  componentDidMount(){
    localStorage.currentStage = 'questions';
  }
  updateInputValue(evt){
    this.setState({selectedValue: evt.target.value });
  }
  tryAgain(){
    this.setState({success:false})
  }
  checkQuestion(){
    if(this.state.selectedValue == '17'){
      localStorage.currentStage = 'application';
      this.setState({success: true})
      return (<Redirect to='/application' />)
    }else{
      this.setState({
        success:'tryagain',
        selectedValue: null,
        firstQuestion: !this.state.firstQuestion,
        secondQuestion: this.state.firstQuestion
      });
      this.setState({selectedValue: null});
    }
  }
  render(){
    // Check to see if they got the correct answer
    if(this.state.success === true){return (<Redirect to='/application' />)}
    else if(this.state.success === 'tryagain'){RenderContent = ContentWrong}
    else if(this.state.success === false){RenderContent = Contents}

    let dis = !this.state.selectedValue;
    return (
      <PageTemplate
        header={<Header />}
        //hero={<Hero />}
        //footer={<Footer />}
      >
        <RenderContent 
          Button={Button}
          buttonDisabled={dis}
          tryAgain={this.tryAgain} 
          checkQuestion={this.checkQuestion}
          Icon={Icon}
          Divider={Divider}
          updateInputValue={this.updateInputValue}
          Heading={<HeadingImage image='WINAPINTONUS' /> }
          Field={FormField}
          secondQuestion={this.state.secondQuestion}
          firstQuestion={this.state.firstQuestion}
        
        />
      </PageTemplate>
    )
  }
}

export default Questions
