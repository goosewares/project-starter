
import React from 'react'
import { Redirect } from 'react-router-dom'

import {
  Button, PageTemplate, Header, Divider, HeadingImage, CountDown, HeroImage,
} from 'components'

import { connect } from 'react-redux'
import moment from 'moment'
import styled, { css } from 'styled-components'
import { palette } from 'styled-theme'
import Sendform from 'store/actions'
import Content from './content.pug'

const Redeem = Sendform.redeem


const check_redeemed = Sendform.check_redeemed

const style = css`
  color: ${palette('primary', 2)};
  font-size: 70px;
  text-transform: uppercase;
  font-weight: bold;
  letter-spacing: 1px;
  margin: 95px 0 20px;
`
const beerCss = css`
  width: 197px;
  top: 192px;
`

const buttonCss = css`
  background-color: transparent;
  border: 1px solid ${palette('primary',4)};
  color: ${palette('primary',4)};
  padding: 15px 10px;
  &:hover{
    background-color: transparent;
    color: ${palette('primary',4)};
  }
`

const StyledContent = styled(Content)`
  .expired{
    ${style}
    margin-top: 100px;
  }
  font-weight: 600;
   .disclaimer {
    font-size: 14px;
    text-align: center;
    display: block;
    font-style: italic;
  }
`
const StyledCountDown = styled(CountDown)`
  ${style}
  margin-top: 110px;
  line-height: 1em;
`
// const StyledCountDown = styled.div`
//   ${style}
//   margin-top: 110px;
//   line-height: 1em;
// `

const Disclaimer = styled.span`
  font-size: 10px;
  display: block;
  margin: 0 30px;
  a {
    font-size: 12px;
    color: white;
  }
`

const redeem = Sendform.redeem
const venue = ''
const currentID = ''
const timerSeconds = 15 * 60
class Order extends React.Component {
  constructor(props) {
    super(props)
    localStorage.currentStage = 'order'
    this.initiatePartyClock()
    this.state = {
      countDownComplete: typeof this.currentMoment !== 'undefined' && this.currentMoment.diff(moment(), 'Seconds') <= 0,
      activated: this.activated,
      currentMoment: this.currentMoment,
      drink_redeemed: false,
      goBack: false,
    }
    this.gettingThePartyClockStarted = this.gettingThePartyClockStarted.bind(this)
    this.countDownEnd = this.countDownEnd.bind(this)
    this.checkRedeemed = this.checkRedeemed.bind(this)
    this.ordered = this.ordered.bind(this)
    this.goBack = this.goBack.bind(this)
  }

  componentWillReceiveProps(nprop) {
    if (!this.props.activateCountdown && nprop.activateCountdown === true) {
      this.startCountdown()
    } else if (nprop.activateCountdown == 'redeemed') {
      this.setState = ({ redeemed: true })
    }
  }

  initiatePartyClock() {
    this.activated = false
    if (typeof localStorage.redeemTime !== 'undefined') {
      this.gettingThePartyClockStarted()
    }
  }

  gettingThePartyClockStarted() {
    // Performing our initial checks to see if the person belongs here or not
    if (typeof sessionStorage.selectedVenue !== 'undefined') {
      // Getting time from storage and doing things with it
      if (typeof localStorage.redeemTime === 'undefined') {
        this.currentMoment = moment().add(timerSeconds, 'seconds')
        localStorage.redeemTime = this.currentMoment.toString()
      } else {
        this.currentMoment = moment(new Date(localStorage.redeemTime))
        if (this.currentMoment.diff(moment(), 'Days') >= 1) {
          this.currentMoment = moment().add(timerSeconds, 'seconds')
          localStorage.redeemTime = this.currentMoment.toString()
        }
      }
      this.activated = true
    }
  }

  countDownEnd(evt) {
    this.setState({
      countDownComplete: true,
    })
  }

  startCountdown() {
    this.gettingThePartyClockStarted();
    this.setState({
      activated: true,
      currentMoment: this.currentMoment,
    });
    window.scrollTo(0, 0);
  }

  checkRedeemed() {
    this.props.check_redeemed(
      { uid: localStorage.currentID }
    )
  }

  ordered() {
    const location = JSON.parse(sessionStorage.selectedVenue)
    this.props.Redeem({
      uid: localStorage.currentID,
      lat: location.location.lat,
      lng: location.location.lng,
      name: location.label,
    })
  }
  goBack(){
    this.setState({goBack: true})
  }
  render() {
    const countDownCompleteActivate = !this.state.countDownComplete && this.state.activated
    if (!sessionStorage.selectedVenue || this.state.goBack) {
      localStorage.currentStage = 'claim'
      return (<Redirect to="/claim" />)
    }
    if (this.props.drink_redeemed == true || this.state.drink_redeemed == true) {
      localStorage.currentStage = 'redeemed'
      return (<Redirect to="/redeemed" />)
    }
    return (
      <PageTemplate
        header={<Header />}
        hero={<HeroImage image="/images/BG.png" stylecss={beerCss} />}
        // footer={<Footer />}
      >

        <StyledContent
          CountDown={StyledCountDown}
          countDownCompleteActivate={countDownCompleteActivate}
          countDownComplete={this.state.countDownComplete}
          toDate={this.state.currentMoment}
          GoBack={this.goBack}
          onCountdownEnd={this.countDownEnd}
          timerActivated={this.state.activated}
          Button={Button}
          ButtonCss={buttonCss}
          sendActivation={this.checkRedeemed}
          pintOrdered={this.ordered}
          Divider={Divider}
          Heading={<HeadingImage image="ORDERYOURFREEGUINNESS" />}
        />

      </PageTemplate>
    )
  }
}

export default connect(
  state => ({
    drink_redeemed: state.sendform.drink_redeemed,
    selectedVenue: state.sendform.selectedVenue,
    activateCountdown: state.sendform.activateCountdown,
  }),
  {
    Redeem,
    check_redeemed,
  }
)(Order)
