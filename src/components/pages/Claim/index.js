
import React from 'react'

import styled from 'styled-components'
import { font, palette } from 'styled-theme'

import Content from './content.pug'
import { Button, PageTemplate, Header, Divider, HeadingImage, Icon, AutoComplete } from 'components'
import { GetLocation } from 'components/utilities'
import locations from 'locations'
import * as R from 'ramda'
import {connect} from 'react-redux'
import SendForm from 'store/actions'
import {Redirect} from 'react-router-dom'
const saveLocation = SendForm.saveLocation;

class Claim extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loc: false,
      selectedLocation: false,
      selectedNear: false,
      selectedSearch: false,
      selectedVenue: false,
      goOrder: false,
      venueSearchUniqueId: this.createID(),
      venueNearUniqueId: this.createID()
    }
    this.location = this.location.bind(this)
    this.locationReject = this.locationReject.bind(this)
    this.selectedVenue = this.selectedVenue.bind(this)
    this.buttonClick = this.buttonClick.bind(this)
  }

  location(location){
    this.setState({loc:{
      lat: location.coords.latitude,
      lng: location.coords.longitude,
    }})
  }
  createID(){
    return Math.ceil(Math.random()*100000);
  }
  distance(coord1,coord2){
    if( (coord1.lat==coord2.lat) && (coord1.lng==coord2.lng) ){
      return 0
    }
    let radlat1 = Math.PI * coord1.lat/180
    let radlat2 = Math.PI * coord2.lat/180
    let theta = coord1.lng-coord2.lng
    let radtheta = Math.PI * theta/180
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    if(dist>1){
      dist=1;
    }
    dist = (Math.acos(dist)*180/Math.PI) * 60 * 1.1515;
    return dist * 1609.344 
  }

  locationReject(suggest){
    if(this.state.loc){
      return this.distance(this.state.loc,suggest.location) < 1000;
    }else{
      return [{label:'nothing'}]
    }
  }
  getLocationSuggestions(){
    if(this.state.loc){
      const location = this.state.loc;
      let newLocations = R.pipe(
        R.map(o=>{
          let distance = this.distance(location,o.location);
          if(distance<10000){
            return R.mergeDeepLeft({distance:distance},o)
          }else{
            return null;
          }
        }),
        R.filter(o=>o != null),
        R.sortBy(R.prop('distance')),
        R.take(5)
      )(locations)
      return newLocations;
    }else{
      return [{label:'nothing'}];
    }
  }
  componentDidMount(){
    GetLocation(this.location,fail=>{console.log('no location here');});
  }

  selectedVenue(fieldId,suggest){
    const newState = {
      selectedVenue: JSON.stringify(suggest)
    };
    newState[fieldId] = true;
    this.setState(newState);
  }

  buttonClick(evt){
    sessionStorage.selectedVenue = this.state.selectedVenue;
    this.setState({goOrder:true})
  }

  render() {
    if(this.state.goOrder){
      localStorage.currentStage = 'order';
      return (<Redirect to='/order' />)
    }
    const ContentProps = {
      Button: Button,
      ButtonClick:this.buttonClick,
      buttonDisabled:!this.state.selectedVenue ? true:false,
      HeadingImage:HeadingImage,
      Divider:Divider,
      Icon:Icon,
      Geosuggest:AutoComplete,
      geoSuggestProps: {
        searchNear: {
          active: this.state.loc,
          list: this.getLocationSuggestions(),
          placeholder: 'Venues near me',
          onSelect: ((s,o)=>{
            this.selectedVenue("selectedNear",o)
            this.setState({venueSearchUniqueId:this.createID()})
          }).bind(this),
          openOnClick: true,
          image: '/images/DownArrowIcon.png',
          key: this.state.venueNearUniqueId
        },
        searchVis:{
          placeholder:'Search venue',
          list: locations,
          openOnClick: false,
          onSelect:((s,o)=>{
            this.selectedVenue("selectedSearch",o)
            this.setState({venueNearUniqueId:this.createID()})
          }).bind(this),
          image:'/images/SearchIcon.png',
          key: this.state.venueSearchUniqueId,
          minLength: 2
        }
      },
      selectedVenue:this.selectedVenue,
    }

    return (
      <PageTemplate
        header={<Header />}
        //hero={<Hero />}
        //footer={<Footer />}
      >
        <Content {...ContentProps}/>
      </PageTemplate>
    )
  }
}

export default connect(
  state=>{
    return ({selectedVenue: state.sendform.selectedVenue})
  },
  {saveLocation}
)(Claim)
