import React from 'react'
import { Link, Button, PageTemplate, Header, Hero, Footer, Divider, HeroImage } from 'components'
import styled from 'styled-components'
import {palette} from "styled-theme";

const Img = styled.img`
  margin-top: 180px;
  padding-bottom:8px;
  width: 266px;
`

const Lefty = styled.span`
  text-align: left;
  word-break: break-word;
  a{
    text-transform: none;
    font-size: inherit;
    color: ${palette('primary',4)};
  }
  p{
    padding-bottom: 20px;
  }
`
const HowToPlay = () => {
  return (
    <PageTemplate
      header={<Header />}
      hero={<HeroImage image="/images/BG.png" />}
      //footer={<Footer />}
    >
      <Img src='images/headings/STOUT_SHOUT.svg' />
      <Divider/>
        <Lefty>
        <p><em>Q: The Pub Finder site isn't loading. What should I do?</em><br />
        A: If the Stout Shout promotional website is not loading, there are a couple of preliminary steps you can take.
        <br /><br />
        First, please check your device is properly connected to the internet. If you are connected to WIFI, please try
        disconnecting WIFI and re-loading the promotional site on 4G or loading another webpage (i.e. google.com.au)
        to ensure your internet connection is stable.
        <br /><br />
        If internet connection is not the cause of the issue, we encourage you to clear the browsing history on your
        device and open the promotion page in a new tab; preferably in a new private tab or a new browser. This will
        ensure there are no other conflicting browser settings interrupting with the promotion.
        <br /><br />
        Should you still be experiencing issues, please contact the Consumer Contact Centre on <a href="tel:1800308388">1800 308 388</a> or Guinness
        Facebook page with a detailed overview, URLs and accompanying images of the trouble experienced and a member
        of our team will be in touch.</p>

        <p><em>Q: My timer has run out but I didn't get to redeem. How can I restart it again?</em><br />
        A: If your timer has expired, you'll need to come back and re-enter the promotion again the
        next day. We'll remember your device details and you can re-activate the offer when ready to order at the bar.</p>

        <p><em>Q: When I reopened the site, it doesn't work? I've lost my free pint offer.</em><br />
        A: Don't worry, if you have entered the promotion but not yet redeemed your pint, we'll be
        able to track your entry. Please try going through the promotion steps again. Once y
        ou have filled out your personal details, we'll remember your mobile number and you'll
        be able to select which venue you would like to redeem at.</p>

        <p><em>Q: I don't have a smartphone / My phone doesn't work! How can I still enter?</em><br />
        A: Unfortunately, you will need a mobile or tablet device to enter this promotion as
        you will need to show the activated promotion (touch) screen to the participating venue bartender to receive your free pint of Guinness.</p>

        <p><em>Q: It's not allowing me to enter the competition! What should I do?</em><br />
        A: We have put together some instructions for how to enter the promotion which you
        can find here: <a href="https://www.stoutshout.com.au/how-to-enter">www.stoutshout.com.au/how-to-enter</a> Should you still be experiencing
            issues, please contact the Consumer Contact Centre on <a href="tel:1800308388">1800 308 388</a> team with detailed
            overview, URLs and accompanying images of the trouble experienced and a member of our team will be in touch.</p>

        <p><em>Q: Does the Guinness Stout Shout work on all devices? iPhone, Android etc</em><br />
        A: The promotion is optimised to work across the following devices:<br />
        iOS Devices; iPhone X, iPhone 7 Plus, iPhone 6<br />
        Android Devices; Google Pixel 2 XL, Samsung S8, Samsung S7<br />
        Tablets; iPad Pro 9<br /><br />
        The promotion isn't accessible across desktop browsers so we encourage you to participate on a supported mobile device.</p>

        <p><em>Q: What mobile devices are incompatible for the promotion?</em><br />
        A:The promotion is optimised to work across the following devices:<br />
        iOS Devices; iPhone X, iPhone 7 Plus, iPhone 6<br />
        Android Devices; Google Pixel 2 XL, Samsung S8, Samsung S7<br />
        Tablets; iPad Pro 9<br /><br />

        The promotion isn't accessible across desktop browsers so we encourage you to participate on a supported mobile device.</p>

        <p><em>Q: The site has crashed. When will it be fixed?</em><br />
        A: If the Stout Shout promotional website is not loading, there are a couple of preliminary steps you can take.<br />

        First, please check your device is properly connected to the internet. If you are connected to WIFI, please try
        disconnecting WIFI and re-loading the promotional site on 4G or loading another webpage (i.e. google.com.au) to
        ensure your internet connection is stable.<br />

        If internet connection is not the cause of the issue, we encourage you to clear the browsing history on
        your device and open the promotion page in a new tab; preferably in a new private tab or a new browser.
        This will ensure there are no other conflicting browser settings interrupting with the promotion.<br />

        Should you still be experiencing issues, please contact the Consumer Contact Centre on <a href="tel:1800308388">1800 308 388</a>
        with detailed overview, URLs and accompanying images of the trouble experienced and a member of our team will be in touch.</p>

        <p><em>Q: I'm getting an error. What should I do?</em><br />
        A: If you are experiencing an error when filling in the entry form, please try clearing your device browsing history and starting again.
            If the error persists, please contact the Consumer Contact Centre on <a href="tel:1800308388">1800 308 388</a> with detailed overview, URLs and accompanying images
        of the trouble experienced and a member of our team will be in touch.</p>

        <p><em>Q: Does it matter if a consumer chooses one venue to redeem in the app but then takes it to a different participating pub to redeem? Functionally can they go back and amend?</em><br />
        A: If you make a mistake on the venue selected,
            you will have the opportunity to go back and change your venue of choice. This can only be
        done if you have not yet activated your offer in venue. Be sure to only activate your offer
        when you are at the bar ready to redeem otherwise your offer may expire and you will need to wait until the next day to try again.</p>

        <p><em>Q: I could not enter the promo because the Guinness keg had run out at the venue selected. Not happy!</em><br />
        A: If you are within the promotional dates then the venue will honour your redemption of a free pint. If you
        have the issue again please let us know the venue name and we will sort it out for you.</p>

                                                                                           <p><em>Can I redeem on St. Patrick's Day?</em><br />
        The Guinness Stout Shout promotion is only live in the lead up to the St.
            Patrick's Day weekend (02/03/19 - 14/03/19). Make sure you head out to celebrate
        over the St. Patrick's Day weekend as many Guinness venues will have other St.
        Patrick's Day promotions running. Slainte!</p>

        <p><em>Q: I can't enter the promo.</em><br />
        A: Have you already redeemed a free Guinness pint today? According to the T&Cs
        only one (1) Guinness pint is redeemable each day during the promotional period.</p>

        <p><em>Q: I don't like Guinness. Can I redeem for something else?</em><br />
        A: The Guinness Stout Shout promotion is for Guinness draught only.</p>

        <p><em>Q: Why do I need to enter my details?</em><br />
        A: In the interest of the most amount of people being able to claim their free
        pint of Guinness, we use your details to cross-check whether you have already
        claimed a pint. We will also reach out to you after St. Patrick's Day with any
        news or offers that we feel you may be interested in.  You can opt-out from hearing
        anything further from Guinness at any time.</p>

        <p><em>Q: What are you doing with my data?</em><br />
        A: Your data is encrypted and stored securely on our server. We also use your
        data to cross-check whether you have claimed a pint in accordance with the promotion
        T&C's, and reach out to you after St. Patrick's Day with any news or offers that we
        feel you may be interested in.  You can opt-out of hearing from Guinness at any time.</p>

        <p><em>Q: The barman didn't know anything about the promo and wouldn't give me my free pint!</em><br />
        A: Please send an email to LionAU-Enquiries@Lionco.com to let us know the name of the venue and we will sort it out for you.</p>

                                                                                                                                <p><em>Q: I accidentally started the timer before I was at the venue. Can I still redeem my free pint?</em><br />
        A: If you have prematurely started your timer and it has run out there's no need to stress. You can restart
        the timer in 24 hours - just make sure you only tap to start the timer when you're in line at the bar so it doesn't run out again.</p>

        <p><em>Q: I went to a venue on SPD weekend and they wouldn't let me redeem my pint.</em><br />
        A: The promotional dates of the Guinness Stout Shout are 02/03/19 - 14/03/19. Here is a link to the T&Cs.</p>

        <p><em>Q: How do I know which venues are participating in the Guinness Stout Shout?</em><br />
        A: During the promotional period if you visit <a href="https://www.stoutshout.com.au">stoutshout.com.au</a> all the participating venues will appear green whilst stocks last.</p>

            <p><em>Q: Which venues are participating in my state?</em><br />
        A: Here is the full list of participating venues per state:<br /><br />

        <strong>VIC</strong><br />
        PJ O'Briens Melbourne<br />
        The Drunken Poet<br />
        The Irish Times<br />
        The Quiet Man Irish Pub<br />
        The 5th Province<br />
        Jimmy O Neills<br />
        Irish Murphy's<br />
        The Brothers Public House<br />
        Elephant & Wheel Melb<br />
        Bridie O'reillys South Yarra<br /><br />

        <strong>NSW</strong><br />
        PJ O'Briens Sydney<br />
        Mercantile Hotel Sydney<br />
        Maloneys Hotel Sydney<br />
        Porterhouse Surry Hills<br />
        Fortune Of War Hotel<br />
        King Omalleys<br />
        Scruffy Murphys<br />
        Northern Star Hotel Hamilton<br />
        Carrington Hotel Katoomba<br />
        Hero Of Waterloo Hotel Millers Point<br />
        Penrith Gaels Soccer Club & Culture Association<br />
        Kellys On King Newtown<br />
        The Riverview Hotel<br />
        Mean Fiddler Rouse Hill<br />
        Dicey Rileys Hotel<br /><br />

        <strong>QLD</strong><br />
        Finn Mccools Irish Bar<br />
        Irish Murphy's<br />
        Waxy's Irish Pub<br />
        Paddy's Port Douglas<br />
        PJ O'Briens Cairns<br />
        Gilhooley's Brisbane<br />
        Darcy Arms<br />
        Irish Club Hotel Toowoomba<br /><br />

        <strong>WA</strong><br />
        Durty Nellys<br />
        J B O'reilly's<br />
        Murphy's Irish Pub<br />
        Fibber McGees Leederville<br />
        Paddy Malones<br />
        The Duke Bar and Bistro<br />
        Woodbridge Hotel Guildford<br />
        Rosie O'Grady's Northbridge<br />
        Irish Club of WA<br />
        Friar Tucks<br />
        Paddy Maguires<br />
        The Height's Bar & Bistro<br />
        National Hotel Fremantle<br />
        The Woodvale Tavern</p>

        <p><em>Q: Why is my closest/favourite Guinness pub not participating in the Free Pint promo?</em><br />
        A: There a select group of venues participating in the Guinness Stout Shout. During the
        promotional period if you visit <a href="https://www.stoutshout.com.au">stoutshout.com.au</a> all the participating venues will appear green.</p>

            <p><em>Q: Why do I need to purchase a main meal in order to redeem my pint?</em><br />
        A: According to the Liquor Licensing laws in Western Australia free alcohol cannot
        be supplied without the purchase of a main meal.</p>

        <p><em>Q: How many pints can I redeem?</em><br />
        A: This promotion only allows the redemption of one pint per person.</p>
    </Lefty>
    </PageTemplate>
  )
}

export default HowToPlay
