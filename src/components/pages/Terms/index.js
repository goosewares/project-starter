import React from 'react'
import { Link, Button, PageTemplate, Header, Hero, Footer, Divider, HeroImage } from 'components'
import styled from 'styled-components'
import {palette} from "styled-theme";

const Img = styled.img`
  margin-top: 180px;
  padding-bottom:8px;
  width: 266px;
`

const Lefty = styled.span`
  text-align: left;
  word-break: break-word;
  a{
    text-transform: none;
    font-size: inherit;
    color: ${palette('primary',4)};
  }
`
const Terms = () => {
  return (
    <PageTemplate
      header={<Header />}
      hero={<HeroImage image="/images/BG.png" />}
      //footer={<Footer />}
    >
      <Img src='images/headings/STOUT_SHOUT.svg' />
      <Divider/>
        <h2>Terms and Conditions</h2>
        <Lefty>
            <p><strong>Promotion: </strong>
            Guinness complimentary pint 1027</p>

            <p><strong>Promoter: </strong>
            Lion - Beer, Spirits & Wine Pty Ltd ABN 13 008 596 370, Level 7, 68 York St, Sydney, NSW 2000, Australia. Ph: <a href="tel:1800677852">1800 677 852</a></p>

            <p><strong>Promotional Period: </strong><br />
                <strong>Start date: </strong>02/03/19 at 12:00 pm AEDT<br />
                <strong>End date: </strong> 14/03/19 at 11:59 pm AEDT or once all gifts available are exhausted</p>

            <p><strong>Eligible claimants: </strong>
            Entry is only open to NSW, QLD, VIC and WA residents who are 18 years and over.</p>

            <p><strong>How to Claim: </strong>
            To be eligible to claim a gift, the claimant must complete all the steps:<br /><br />

            1.	Visit <a href="http://www.stoutshout.com.au">http://www.stoutshout.com.au</a> on their compatible mobile/tablet device, follow the prompts to the Promotion page; play the Multiple choice question game (must answer question correctly). Claimants may &quot;try again&quot; if they obtain an incorrect answer; and
                <br /><br />2.	Then fully complete the online claim form with their personal details (first name, surname, phone number, date of birth, email address and postcode), select the option ‘redeem now’ or ‘save for later’, select the tick box agreeing to have read and understood and agree to the Promoter’s Privacy Policy and these Terms and Conditions; tick the box consenting to receiving marketing communications from Guinness via email; and submit the fully completed claim form.

            <br /><br />If the claimant selects the &quot;redeem now&quot; option they will be then be required to select the venue where they would like to claim their free pint gift (from the list of available venues). Once a venue is selected they must then select &quot;redeem at bar&quot;. Once the &quot;redeem at bar&quot; option is selected the claimant has 15 minutes to claim the free pint gift. The claimant must present the &quot;activate&quot; offer screen of the offer to a bartender at the venue selected for redemption within the 15 minutes. The bartender will then select the &quot;activate&quot; button to confirm the free pint gift has been awarded. If the claimant does not claim within the 15 minutes they will need to wait another 24 hours before they can attempt to claim again.

            <br /><br />If the claimant selects the &quot;save for later&quot; option, they can close the webpage and return to the webpage on their compatible mobile/tablet device used to complete the claim form, and then follow the prompts as per the above to select a venue to redeem and then follow the prompts to redeem. If a claimant clears their device cache and history during the Promotional Period, the claimant will need to recomplete the multiple choice questions and claim form again. If the claimant has already redeemed their free pint gift they will not be able to redeem again.

            <br /><br />The above must be completed during the Promotional Period to be eligible to claim the free pint gift.

            <br /><br /><strong>In WA:</strong> For WA residents and WA venues, a complimentary pint of Guinness gift will ONLY be provided when a claimant simultaneously buys a substantial meal at the venue at the time of gift redemption.

            <br /><br />Claimant will only be awarded the gift if the the member of staff serving them deems the claimant fit for alcohol consumption under RSA guidelines.

            <br /><br /><strong>There is a limit of one (1) keg of Guinness Draught available for redemption as part of this offer in each Participating Venue. Once the full keg of Guinness Draught is redeemed by claimants, this offer will not be able to be redeemed by any further claimants within the same Participating Venue.</strong>

            </p>

            <p><strong>Participating Venues: </strong>
            Participating venues are any venues specified as participating in this Promotion, as advertised at <a href="http://www.stoutshout.com.au">http://www.stoutshout.com.au</a>

            </p>

            <p><strong>Gift: </strong>
            Claimants will receive one (1) x 570ml Pint Glass of Guinness Draught (4.1%) valued at $10 RRP.

            </p>

            <p><strong>Claims permitted: </strong>
            Limit one (1) claim and gift permitted per person, email address and phone number.</p>

            <hr />

            <p>1.	The claimant agrees and acknowledges that they have read these Conditions of Claim (and Schedule) and that claiming a gift in the Promotion is deemed to be acceptance of these Conditions of Claim (and Schedule). Any capitalised terms used in these Conditions of Claim have the meaning given in the Schedule, unless stated otherwise. Offer not valid in conjunction with any other offer.
            </p><p>2.	The Promotion commences on the Start Date and ends on the End Date ("Promotional Period"). Claims are deemed to be received at the time of receipt by the Promoter and not at the time of transmission or deposit by the claimant. Records of the Promoter and its agencies are final and conclusive as to the time of receipt.
            </p><p>3.	Valid and eligible claims will only be accepted during the Promotional Period, while gift last.
            </p><p>4.	Employees (and the immediate family members) of agencies/companies directly associated with the conduct of this Promotion, the Promoter, its distributors, suppliers, subsidiary companies/businesses and associated companies and agencies are not eligible to claim. "Immediate family member" means any of the following: spouse, ex-spouse, de-facto spouse, child or step-child (whether natural or by adoption), parent, step-parent, grandparent, step-grandparent, uncle, aunt, niece, nephew, brother, sister, step-brother, step-sister or 1st cousin.
            </p><p>5.	Tickets or rights for alcohol prizes will not be distributed by or to any person under 18, nor can a person under 18 dispense or collect an alcohol prize. Claimants will be refused service of alcohol or provision of an alcoholic beverage prize if it would breach any relevant laws or codes including those relating to the responsible service of alcohol. If a claimant is deemed as unfit for the consumption of alcohol under the RSA guidelines by bar staff at the Participating Venue, the claimant will not be able to make a claim. The Promoter supports the responsible service of alcohol.
            </p><p>6.	No part of a gift is exchangeable, redeemable for cash or any other prize or transferable, unless otherwise specified in writing by the Promoter.
            </p><p>7.	If there is a dispute as to the identity of a claimant, the Promoter reserves the right, in its sole discretion, to determine the identity of the claimant.
            </p><p>8.	Claimants' personal information will be collected by the Promoter. Personal information will be stored on the Promoter's database. The Promoter is bound by the Australian Privacy Principles in accordance with the Privacy Act 1988 (Cth) and its privacy policy which is located at <a href="https://www.lionco.com/legal/privacy-policy">https://www.lionco.com/legal/privacy-policy</a>. The Promoter's privacy policy contains information about how the entrant may access, update and seek correction of the personal information the Promoter holds about them and how the entrant may complain about any potential breach by the Promoter of the Australian Privacy Principles or any other Australian privacy laws and how such complaints will be dealt with. The Promoter collects personal information about claimants to enable them to participate in this Promotion and may disclose the claimants' personal information to third parties including its contractors and agents, prize suppliers and service providers to assist in conducting this Promotion. If the claimant does not provide their personal information as requested, they may be ineligible to enter or claim a prize in the Promotion. Personal information collected from claimants will not be disclosed to any entity located outside of Australia.
            </p><p>9.	Any guarantee or warranty given is in addition to any relevant statutory guarantees and warranties and nothing in these Conditions of Claim restricts, excludes or modifies or purports to restrict, exclude or modify any statutory consumer rights under any applicable law including the Competition and Consumer Act 2010 (Cth).
            </p><p>10.	The Promoter supports the responsible service of alcohol and encourages consumers to enjoy alcohol responsibly. Entrants will be refused service of alcohol or provision of an alcohol beverage if it would breach any laws, codes or policies including those of the relevant liquor licensee relating to the responsible service of alcohol. Legal aged consumers are advised to consider the safe drinking levels recommended in the National Health and Medical Research Council Australian Guidelines to Reduce Health Risks from Drinking Alcohol. A full version of these Guidelines is available at <a href="https://nhmrc.gov.au/about-us/publications/australian-guidelines-reduce-health-risks-drinking-alcohol">https://nhmrc.gov.au/about-us/publications/australian-guidelines-reduce-health-risks-drinking-alcohol</a>. Please refer to the GL4001 &quot;Liquor promotion guidelines&quot; and GL4003 &quot;Intoxication guidelines&quot; at liquorandgaming.justice.nsw.gov.au
            </p><p>11.	If for any reason any aspect of this Promotion is not capable of running as planned, including by reason of computer virus, communications network failure, bugs, tampering, unauthorised intervention, fraud, technical failure or any cause beyond the control of the Promoter, the Promoter may in its sole discretion cancel, terminate, modify or suspend the Promotion and invalidate any affected entries, or suspend or modify a gift.
            </p><p>12.	The Promoter reserves the right, at any time, to validate and check the authenticity of claims and claimant's details (including a claimant's identity, age and place of residence). In the event that a claimant cannot provide suitable proof as required by the Promoter to validate their entry, the claimant will forfeit the gift in whole and no substitute will be offered. Incomplete, indecipherable, inaudible, incorrect and illegible claims, as applicable, will at the Promoter's discretion be deemed invalid and not eligible to claim a gift. The use of any automated entry software or any other mechanical or electronic means that allows an individual to automatically claim repeatedly is prohibited and may render all claims submitted by that individual invalid. The Promoter reserves the right to disqualify any individual who provides false information, fails to provide information, conspires with others to gain an unfair advantage or is otherwise involved in any conduct that involved manipulating, interfering or tampering with this promotion or otherwise preventing the conduct of the promotion as intended by the Promoter.
            </p><p>13.	The Promoter reserves the right to disqualify claims in the event of non-compliance with these Conditions of Claim. In the event that there is a dispute concerning the conduct of the Promotion, the decision of the Promoter is final and binding on each claimant and no correspondence will be entered into.
            </p><p>14.	The Promoter and its associated agencies and companies will not be liable for any loss (including, without limitation, indirect, special or consequential loss or loss of profits), expense, damage, personal injury or death which is suffered or sustained (whether or not arising from any person's negligence or wilful misconduct) in connection with this Promotion or accepting or using any gift (or recommendation), except for any liability which cannot be excluded by law (in which case that liability is limited to the minimum allowable by law).
            </p><p>15.	The claimant will participate in and co-operate as required with all reasonable marketing and editorial activities relating to the Promotion, including (but not limited to) being recorded, photographed, filmed or interviewed and acknowledges that the Promoter may use any such marketing and editorial material without further reference or compensation to them.
            </p><p>16.	The Promoter accepts no responsibility for any tax implications and the claimant must seek their own independent financial advice in regards to the tax implications relating to the gift or acceptance of the gift.
            </p><p>17.	Failure by the Promoter to enforce any of its rights at any stage does not constitute a waiver of these rights.</p>
        </Lefty>

    </PageTemplate>
  )
}

export default Terms
