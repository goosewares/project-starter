import React from 'react'
import { Link, Button, PageTemplate, Header, Hero, Footer, Divider, HeroImage } from 'components'
import styled, { createGlobalStyle } from 'styled-components'

const Img = styled.img`
  margin-top: 180px;
  padding-bottom:8px;
  width: 266px;
  
`
const Span = styled.span`
  margin: 0 30px;
  display: block;
  font-weight: 600;
`
const Disclaimer = styled.span`
  font-size: 12px;
  display: block;
  margin: 0 30px;
  a {
    font-size: 12px;
    color: white;
  }
`
const HomePage = () => {
  localStorage.currentStage = 'home'
  return (
    <PageTemplate
      header={<Header />}
      hero={<HeroImage image="/images/BG.png" />}
      //footer={<Footer />}
    >
      <Img src='images/headings/STOUT_SHOUT.svg' />
      <Divider/>
      <Span>Take our simple quiz to redeem your FREE Guinness at participating venues.</Span>
      <br/><Link href="/how-to-enter" underlined='true' uppercase='true'>How to enter</Link>
      <p><br/><Button to='/questions'>Get Started</Button></p>
      <Disclaimer><em>NSW, QLD, VIC, WA resid. 18+ only. Ends 11.59pm AEDT 14/3/19, while stocks last in venue. <Link href="/terms">T&Cs apply.</Link></em></Disclaimer>
    </PageTemplate>
  )
}

export default HomePage
