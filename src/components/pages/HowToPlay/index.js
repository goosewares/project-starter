import React from 'react'
import { Link, Button, PageTemplate, Header, Hero, Footer, Divider, HeroImage } from 'components'
import styled from 'styled-components'
import {palette} from "styled-theme";

const Img = styled.img`
  margin-top: 180px;
  padding-bottom:8px;
  width: 266px;
`
const Para = styled.p`
  a{
    text-transform: none;
    font-size: inherit;
    color: ${palette('primary',4)};
  }
`

const HowToPlay = () => {
  return (
    <PageTemplate
      header={<Header />}
      hero={<HeroImage image="/images/BG.png" />}
      //footer={<Footer />}
    >
      <Img src='images/headings/STOUT_SHOUT.svg' />
      <Divider/>
        <h3>HOW TO ENTER</h3>
    <p>Entering is as easy as 1, 2, 3!</p>

    <p>1. Visit www.stoutshout.com.au</p>
    <p>2. Answer one multiple choice question (trust us, it’s an easy one!) and fill in your details</p>
    <p>3. Redeem your free pint at a participating venue</p>

    <p>You’ll be given an option to ‘Redeem Now’ or ‘Save for later’. We recommend only
    activating ‘redeem now’ if you are already at the bar ready to claim your pint,
        otherwise your offer will expire after 15 minutes, and you’ll have to wait until the
    next day to try again.</p>
    <Para>Need help troubleshooting? Visit our <a href="/faq">Frequently Asked Questions</a>.</Para>
    <Para><a href="/terms">Click here</a> for the full promotional terms and conditions.</Para>



    </PageTemplate>
  )
}

export default HowToPlay
