
import React from 'react'
import Content from './content.pug'
import { Link, Button, PageTemplate, Header, Divider, HeadingImage, Footer, ReduxField, Test, Icon } from 'components'
import styled,{css} from 'styled-components'
import { font} from 'styled-theme'

import {connect} from 'react-redux'
import {Field, reduxForm } from 'redux-form'
import SendForm from 'store/actions'
import Recaptcha from 'react-recaptcha'
import config from 'config'
import * as validate from 'components/utilities/Validation'
import {Redirect} from 'react-router-dom'


const StyledContent = styled(Content)`
  .hintText{
    text-align: left;
    font-size: 14px;
  }
  padding: 0 20px;
  .disclaimer {
    font-size: 14px;
    text-align: left;
    //font-size: 12px;
    display: block;
    font-style: italic;
  }
`
const LinkStyle = css`
  color: #fff;
  font-family: ${font('primary')};
  font-size: 1em;
  text-transform: none;
  text-decoration: underline
`
const sendAction = SendForm.sendForm;

const Form = reduxForm({
  form: 'application'
})(StyledContent)


class ApplicationForm extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      captcha: false,
      captchaEnabled: false,
      goNext: false,
      saved: false,
    }
    this.captchaCallback = this.captchaCallback.bind(this)
    this.sendAction = this.sendAction.bind(this)
  }
  captchaCallback(response){
    console.log(response);
    this.setState({captcha:response,captchaEnabled:true})
  }
  sendAction(values){
    // console.log(values,this.props,this.state)
    // if(this.state.captchaEnabled){
    //   values.recaptcha = this.state.captcha;
      values.phone = values.phone_number.replace(/ /ig,'').replace(/^0/,'61');
        this.setState({
          saved: values.redeem == 'save',
        });
      
      this.props.sendAction(values);
    // }
  }
  render(){
    if(this.props.form_sent){
      localStorage.currentStage = 'claim';
      if(this.state.saved){
        localStorage.currentStage = 'saved';
        return (<Redirect to='/saved' />)
      }
      return (<Redirect to='/claim' />)
    }
    if(this.props.drink_redeemed){
        localStorage.currentStage = 'redeemed'
        return (<Redirect to='/redeemed' />)
    }
    const formVars = {
      Icon:Icon,
      Button:Button,
      onSubmit:this.sendAction,
      Divider:Divider,
      Heading:(<HeadingImage image='CONGRATULATIONS' />),
      PrivacyPolicyLabel: (<span>I have read, understood and agree to the <Link overwriteStyle={LinkStyle} href="http://lionco.com/legal/privacy-policy" target="_blank">Privacy Policy</Link> & <Link overwriteStyle={LinkStyle} href="/terms" target="_blank">Terms and Conditions.</Link>*</span>),
      Field:Field,
      Test:Test,
      Recaptcha:Recaptcha,
      captcha:this.state.captchaEnabled,
      validator:validate,
      Config:config,
      FormField:ReduxField,
    };
    return (
      <PageTemplate
        header={<Header />}
        //hero={<Hero />}
        //footer={<Footer />}
      >
        {this.state.captcha}
        <Form {...formVars} />
      </PageTemplate>
    )
  }
}

export default connect(
  state=>({
    form_sent: state.sendform.form_sent,
    drink_redeemed: state.sendform.drink_redeemed
  }),
  {sendAction}
)(ApplicationForm)
