import React from 'react'
import { Redirect } from 'react-router-dom'

import Content from './content.pug'
import { Link, Button, PageTemplate, Header, Divider, HeadingImage, Icon } from 'components'

import moment from 'moment'
import styled, {css} from 'styled-components'
import { palette, font } from 'styled-theme'

const StyledContent = styled(Content)`
 h2{
   margin-bottom: 1px;
 }
  .tags{
    min-height: 100%;
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: center;
  }
  .tags > div {
    display: flex; 
    flex-basis: 150px;  
    justify-content: center;
    flex-direction: column;
  }
  .tags a {
    font-size: 14px;
    text-align: center;
    text-transform: none;
    font-family: sans-serif;
    color: white;
    font-weight: lighter;
    line-height: 18px;
  }
  
   .tags img {
    padding-bottom: 10px;
  }

  .redeemed-content {
    padding: 0 25px;
    font-weight: bold;
  }
  .slainte {
    line-height: 3;
  }
`
const linkCss = css`
  font-family: ${font('primary')};
  font-size: 1em;
  text-transform: none;
  color: ${palette('primary',4)};
`
const buttonCss = css`
  background-color: transparent;
  border: 1px solid ${palette('primary',4)};
  color: ${palette('primary',4)};
  padding: 10px 10px;
  &:hover{
    background-color: ${palette('primary',4)};
    color: #000;
  }
`

class Redeemed extends React.Component {
  constructor(props){
    super(props)
    localStorage.currentStage = 'redeemed';
  }
  componentDidMount(){
  }
  render(){
    return (
      <PageTemplate
        header={<Header />}
        //hero={<HeroImage image="/images/BG.png" stylecss={beerCss} />}
        //footer={<Footer />}
      >
        <StyledContent
          Button={Button}
          Divider={Divider}
          Icon={Icon}
          Link={Link}
          LinkCss={linkCss}
          ButtonCss={buttonCss}
          Heading={<HeadingImage image='ENJOYYOURFREEGUINNESS' />}

        />
      </PageTemplate>
    )
  }
}

export default Redeemed
