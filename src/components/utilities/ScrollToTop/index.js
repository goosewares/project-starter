import React from 'react';
import {withRouter} from 'react-router-dom'

class ScrollToTop extends React.Component {
    componentDidUpdate(prevProps) {
        window.scrollTo(0, 0);
    }

    render() {
        // it might be better on this node
        //window.scrollTo(0, 0);
        return this.props.children;
    }
}

export default withRouter(ScrollToTop);