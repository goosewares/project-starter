import moment from 'moment'
module.exports = {
    required : value => (value || typeof value === 'number' ? undefined : 'Required'),
    maxLength : max => value =>
        value && value.length > max ? `Must be ${max} characters or less` : undefined,
    minLength : min => value =>
        value && value.length < min ? `Must be ${min} characters or more` : undefined,
    postcode : value =>
        typeof value !== 'number' && ( value > 9999 || value < 999 )  ? `Must be a valid Australian postcode` : undefined,
    number : value =>{
        console.log('hi');
        return value && isNaN(Number(value)) ? 'Must be a number' : undefined},
    minValue : min => value =>
        value && value < min ? `Must be at least ${min}` : undefined,
    minMax : (min,max)=> value =>{
        console.log(value,min,max,typeof value === 'number' && (value < min || value > max));
        return typeof value !== 'number' && (value < min || value > max) ? 'Must be a valid Australian postcode' : undefined
    }
    ,
    email : value =>
        value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
            ? 'Invalid email address'
            : undefined,
    agegate : value =>{
            if( value && /^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$/i.test(value)  ) {
                let birthday = moment(value, 'DD/MM/YYYY');
                let age = moment.duration(moment().diff(birthday)).asYears();
                return age < 18
                    ? 'You do not meet the minimum age requirement!'
                    : undefined;
            } else {
                return 'Please enter your DOB as DD/MM/YYYY';
            }
    },
    alphaNumeric : value =>
        value && /[^a-zA-Z0-9 ]/i.test(value)
            ? 'Only alphanumeric characters'
            : undefined
    ,
    alphaNumericDash : value =>
        value && /[^a-zA-Z0-9 \-']/i.test(value)
            ? 'Only alphanumeric characters, dashes and apostrophes'
            : undefined
    ,
    hasNo : value =>
      value == ''
        ? 'Please select a state from the dropdown'
        : undefined
    ,
    phoneNumber : value =>
        value && !/^(04[0-9]{8})$/i.test(value.replace(/ /ig,''))
            ? 'Invalid phone number, must be a mobile number with 10 digits'
            : undefined
}
