import React from 'react'
import {Redirect} from 'react-router-dom'

const CanGo = props=>{
    if(typeof localStorage.currentStage != 'undefined' && localStorage.currentStage == props.stage){
        return(<props.Component />)
    }else{
        return(<Redirect to='/' />)
    }
}

export default CanGo