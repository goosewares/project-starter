export default (success,failure) => {
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(success, failure);
    }else{
        failure();
    }
}