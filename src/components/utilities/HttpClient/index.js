import axios from 'axios'
import axiosRetry from 'axios-retry'

axiosRetry(axios,{retryDelay:axiosRetry.exponentialDelay,retries:3})

export default axios;