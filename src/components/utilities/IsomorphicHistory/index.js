import browserHistory from 'history/createBrowserHistory'
import memoryHistory from 'history/createMemoryHistory'

// Making this function like a singleton
let history;
function isomorphicHistory() {
    if(typeof history == 'undefined'){
        history = typeof server != 'undefined' ? memoryHistory() : browserHistory()
    }
    return history
}

export default isomorphicHistory