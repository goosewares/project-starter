import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'
import { ifProp } from 'styled-tools'
import Cleave from 'cleave.js/react'
import * as R from 'ramda'

const fontSize = ({ height }) => `${height / 35.5555555556}rem`

const styles = css`
  font-family: ${font('primary')};
  display: block;
  width: 100%;
  margin: 0;
  box-sizing: border-box;
  font-size: ${fontSize};
  padding: ${ifProp({ type: 'textarea' }, '0.4444444444em', '0 0.4444444444em')};
  height: ${ifProp({ type: 'textarea' }, 'auto', '2.2222222222em')};
  color: ${palette('primary', 0)};
  background-color: transparent;
  border: none;
  border-bottom: 1px solid ${ifProp({invalid:"true"}, palette('danger', 1), palette('primary', 2))};
  &[type=text]:focus {
    background-color: transparent;
  }
  &[type=select]:focus{
    background-color: transparent;
  }
  &[type=checkbox], &[type=radio] {
    display: none;
    + .radio-checkbox {
      top: 4px;
      margin: 0 9px;
    }
  }
  &[type=radio]{
    &:checked + .radio-checkbox:before{
      content: '';
    } 
    + .radio-checkbox {
      display: inline-block;
      border: 1px solid ${ifProp({invalid:"true"}), palette('danger',1), palette('primary',2)};
      border-radius: 50%;
      height: 20px;
      width: 20px;
      position: relative;
      text-align: center;
      &:before {
        background-color: ${palette('primary',2)};
        height: calc(100% - 5px);
        width: calc(100% - 5px);
        border-radius: 50%;
        position: absolute;
        bottom: 50%;
        right: 50%;
        transform: translate(50%,50%);
        display: inline-block;
      }
    }
  }
  
  &[type=checkbox]{
    &:checked + .radio-checkbox:before{
      content: '';
    }
    &[invalid="true"]{
      + .radio-checkbox {
        border-color: ${palette('danger',1)};
      }
    }
    + .radio-checkbox {
      display: inline-block;
      border: 1px solid ${ifProp({invalid:"true"}), palette('danger',1), palette('primary',2)};
      height: 20px;
      width: 20px;
      position: relative;
      text-align: center;
      &:before {
        background-image: url('/images/tick.png');
        background-repeat: no-repeat;
        background-size: contain;
        height: calc(100% - 5px);
        width: calc(100% - 5px);
        position: absolute;
        bottom: 50%;
        right: 50%;
        transform: translate(50%,50%);
        display: inline-block;
      }
    }
  }
`

const StyledTextarea = styled.textarea`${styles}`
const StyledSelect = styled.select`${styles}`
const StyledInput = styled.input`${styles}`
const StyledCleave = styled(Cleave)`${styles}`
const StyledRadioCheckbox = styled((props)=>{
  return (
    <span>
      <input {...props} />
      <span className='radio-checkbox'></span>
    </span>
  )
})`${styles}`
const Input = ({ ...props }) => {
  // filter out unnecessary properties from here
  let prps = R.omit(['invalid'],props)
  prps.invalid = (typeof props.invalid != 'undefined')?props.invalid.toString():'';
  if (props.type === 'textarea') {
    return <StyledTextarea {...prps} />
  } else if (props.type === 'select') {
    return <StyledSelect {...prps} />
  } else if (props.type.match(/(checkbox|radio)/g)){
    return <StyledRadioCheckbox {...prps} />
  } else if(typeof props.options != 'undefined'){
    return <StyledCleave {...prps} />
  }
  return <StyledInput {...prps} />
}

Input.propTypes = {
  type: PropTypes.string,
  reverse: PropTypes.bool,
  height: PropTypes.number,
  invalid: PropTypes.bool,
}

Input.defaultProps = {
  type: 'text',
  height: 40,
}

export default Input