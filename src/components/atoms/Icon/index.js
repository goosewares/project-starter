import React from 'react'
import styled from 'styled-components';
const Icon = styled.div`
    margin: 0px;
`
export default ()=>(
    <Icon><img src="/images/down_arrow.png" /></Icon>
)