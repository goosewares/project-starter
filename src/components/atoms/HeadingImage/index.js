import React from 'react'
import styled from 'styled-components'

const Img = styled.img`
    margin-bottom: 20px;
    width: 310px;
`

export default (props)=>{
    return (
        <Img src={`/images/headings/${props.image}.svg`} />
    )
}