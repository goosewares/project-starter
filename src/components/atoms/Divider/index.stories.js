import React from 'react'
import { storiesOf } from '@storybook/react'
import { Divider } from 'components/atoms'

storiesOf('Divider', module)
  .add('default', () => (
    <Divider name="field" />
  ))
