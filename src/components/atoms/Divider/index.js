import React from 'react'
import Divider from './content.pug'
import styled from 'styled-components'

const Hr = styled(Divider)`
    position: relative;
    margin: 0 0 20px;
    > span {
        width: 75px;
        height: 1px;
        background: -moz-linear-gradient(left, rgba(230,187,98,1) 0%, rgba(230,187,98,0.18) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(left, rgba(230,187,98,1) 0%,rgba(230,187,98,0.18) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to right, rgba(230,187,98,1) 0%,rgba(230,187,98,0.18) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6bb62', endColorstr='#2ee6bb62',GradientType=1 ); /* IE6-9 */
        display: inline-block;
        position: relative;
        vertical-align: middle;
        margin: 0 10px;
        &:first-of-type{
            background: -moz-linear-gradient(left, rgba(230,187,98,0.18) 0%, rgba(230,187,98,1) 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left, rgba(230,187,98,0.18) 0%,rgba(230,187,98,1) 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right, rgba(230,187,98,0.18) 0%,rgba(230,187,98,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2ee6bb62', endColorstr='#e6bb62',GradientType=1 ); /* IE6-9 */
        }
    }
`

export default ()=>(
    <Hr />
)