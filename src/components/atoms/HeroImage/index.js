import React from 'react'
import styled from 'styled-components'
import {prop} from 'styled-tools'

const Hero = styled.div`
    background-image: url(${prop("image")});
    background-repeat: no-repeat;
    background-size: contain;
    max-width: ${prop("maxwidth")};
    width: 100%;
    max-width: 364px;
    height: 100vh;
    top: 0;
    right: 50%;
    transform: translateX(50%);
    z-index: -1;
    position: absolute;
    ${prop('stylecss')}
`
export default props => (
    <Hero {...props}></Hero>
)