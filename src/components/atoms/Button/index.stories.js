import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from '.'
import Button2 from './test.pug';
import { action, configureActions } from '@storybook/addon-actions';

storiesOf('Button', module)
  .add('default', () => {
    let attributes = {
      onClick: action('click')
    }
    return (
      <div>
        <Button>Hello</Button>
        <Button2 attributes={attributes}>
          This is a test children
        </Button2>
      </div>
    );
  })
  .add('reverse', () => (
    <Button reverse>Hello</Button>
  ))
  .add('another palette', () => (
    <Button palette="secondary">Hello</Button>
  ))
  .add('disabled', () => (
    <Button disabled>Hello</Button>
  ))
  .add('transparent', () => (
    <Button transparent>Hello</Button>
  ))
  .add('height', () => (
    <Button height={100}>Hello</Button>
  ))
  .add('link', () => (
    <Button href="https://stoutshout.com.au">Stout Shout</Button>
  ))
