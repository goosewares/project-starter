import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import VideoPlayer from 'react-player'
import R from 'ramda'

const StyledBg = styled.div`
	position: absolute;
	top: 0;
	height: 100%;
	width: 100%;
	z-index: 0;
`;

const Fg = styled.div`
	z-index: 1;
	position: relative;
`;
const Blank = styled.div`
	position: relative;
`;

class Video extends React.Component  {
	constructor(props){
		super(props);
	}
	render(){
		let bgvideo = this.props.bgvideo,
			bgimage = this.props.bgimage,
			Wrapper = this.props.Wrapper,
			children = this.props.children,
			gv = (t) => {this.video = t;},
			onload = () => {
				window.vid = this.video;

			}
		;
		bgvideo = R.mergeDeepRight({
			horizontal: 'center',
			vertical: 'center',
			cover: true,
			ref: (m)=>{this.videoplayer = m;},
			playing: true,
			muted: true,
			loop: true,
			width: '100%',
			height:'100%',
			onReady: onload
		},bgvideo);
		let BgImage = Blank,
			BgVideo = Blank,
			BgVideoInner = Blank,
			top='auto',
			bottom='auto',
			left='auto',
			right='auto',
			Y='-50%',
			X='-50%';
		if(typeof bgimage.image != 'undefined'){
			BgImage = styled.div`
				height: 100%;
				background-size: ${typeof bgimage.size != 'undefined'?bgimage.size:'cover'};
				background-image: url(${bgimage.image});
				background-position:  ${typeof bgimage.vertical!='undefined'?bgimage.vertical:'center'} ${typeof bgimage.horizontal!='undefined'?bgimage.horizontal:'center'} ;
				background-repeat: no-repeat;
			`;
		}
		if(typeof bgvideo.video != 'undefined'){
			if(bgvideo.cover){
				if(bgvideo.horizontal == 'center'){
					left='50%';
				}
				if(bgvideo.horizontal == 'right'){
					right='0';
					X='0';
				}
				if(bgvideo.horizontal == 'left'){
					left='0';
					X='0';
				}
				if(bgvideo.vertical == 'top'){
					top='0';
					Y='0';
				}
				if(bgvideo.vertical == 'bottom'){
					bottom='0';
					Y='0';
				}
				let videoStyle = `
					video {
						min-width: 100%;
						min-height: 100%;
						width: auto !important;
						height: auto !important;
						position: absolute;
						top: ${ top } !important;
						bottom: ${ bottom } !important;
						left: ${ left } !important;
						right: ${right} !important;
						transform: translate(${ X },${ Y });
					}
				`;
				BgVideo = styled(VideoPlayer)`
					position: absolute;
					top: 0;
					bottom: 0;
					width: 100%;
					height: 100%;
					overflow: hidden;
					> ${videoStyle}
				`;
			}else{
				BgVideo = styled(VideoPlayer)`
					position: absolute;
					top: 0;
					bottom: 0;
					width: 100%;
					height: 100%;
					overflow: hidden;
				`;
			}
			let match = /(mp4|webm|ogg)/g,
			urlSplit = x => {
				let test = x.match(match),
					returned = x;
				if(test){ returned = {src:x,type:'video/'+test[0]}; }
				return returned;
			};
			bgvideo.url = (bgvideo.video[0].match(match))?R.map(urlSplit,bgvideo.video):bgvideo.video[0];
			delete bgvideo.horizontal;
			delete bgvideo.vertical;
			delete bgvideo.cover;
		}
		return pug`
Wrapper
	Blank
		StyledBg
			BgImage
				if typeof bgvideo.video != 'undefined'
					BgVideo(...bgvideo)
		Fg
			${ children }
	`;
	}
}
Video.propTypes = {
    children: PropTypes.node,
    bgimage: PropTypes.object,
	bgvideo: PropTypes.object,
}

Video.defaultProps = {
    bgimage: {},
	bgvideo: {},
	Wrapper: Blank
}
export default Video
