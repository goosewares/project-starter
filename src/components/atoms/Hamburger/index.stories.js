import React from 'react'
import { storiesOf } from '@storybook/react'
import Hamburger from '.'
import { action, configureActions } from '@storybook/addon-actions';

storiesOf('Hamburger', module)
  .add('default', () => {
    let attributes = {
      onClick: action('click')
    }
    return (
      <Hamburger />
    );
  })
