import React from 'react'
import styled from 'styled-components'


const Hamburger = styled.img`
    position: absolute;
    right: 20px;
    top: 15px;
`

export default props => {
    const src = props.openHamburger? '/images/close.png':'/images/Burger.png'
    return (
        <Hamburger onClick={props.HamburgerClick} src={src}/>
    )
}
