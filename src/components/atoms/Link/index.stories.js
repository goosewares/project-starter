import React from 'react'
import { storiesOf } from '@storybook/react'
import Link from '.'

storiesOf('Link', module)
  .add('default', () => (
    <Link href="https://stoutshout.com.au">Stout Shout</Link>
  ))
  .add('reverse', () => (
    <Link href="https://stoutshout.com.au" reverse>Stout Shout</Link>
  ))
  .add('another palette', () => (
    <Link href="https://stoutshout.com.au" palette="secondary">Stout Shout</Link>
  ))
