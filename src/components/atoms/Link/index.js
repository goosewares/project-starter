import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { font, palette } from 'styled-theme'
import NavLink from 'react-router-dom/NavLink'
import { ifProp, prop } from 'styled-tools'


let styles = css`
  font-family: ${font('pre')};
  ${ifProp({underlined:"true"},'border-bottom: solid 1px;')}
  ${ifProp({uppercase:"true"},'text-transform: uppercase;')}

  color: ${palette({ grayscale: 0 }, 2)};

  &:hover {
    color: ${palette('primary',2)};
    //text-decoration: underline;
  }

  ${prop('overwriteStyle')}
`
styles = css`
  ${ifProp({is_styled:"true"},styles)}
`


const StyledNavLink = styled(({
  theme, reverse, palette, ...props
}) => <NavLink {...props} />)`${styles}`

const Anchor = styled.a`${styles}`

const Link = ({ ...props }) => {
  if (props.to) {
    return <StyledNavLink {...props} />
  }
  return <Anchor {...props} />
}

Link.propTypes = {
  palette: PropTypes.string,
  underlined: PropTypes.string,
  nostyle: PropTypes.string,
  reverse: PropTypes.bool,
  to: PropTypes.string,
}

Link.defaultProps = {
  palette: 'primary',
  is_styled: "true"
}

export default Link
