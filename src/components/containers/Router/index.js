import React from 'react'
import { Switch, Route } from 'react-router-dom'
import * as actions from 'store/actions'

import * as R from 'ramda'

import {
  HomePage, Questions, ApplicationForm, Claim, Order, Redeemed, HowToPlay, Terms, Faq, SaveForLater
} from 'components/pages'
import { CanGo } from 'components/utilities'

const RouteHome = props => R.cond([
  [o => typeof o === 'undefined' || o == 'home', o => (<HomePage />)],
  [o => o == 'questions', o => (<Questions />)],
  [o => o == 'application', o => (<ApplicationForm />)],
  [o => o == 'saved', o => (<SaveForLater />)],
  [o => o == 'claim', o => (<Claim />)],
  [o => o == 'redeemed', o => (<Redeemed />)],
  [o => o == 'order', o => (<Order />)],
  [o => o == 'howtoplay', o => (<HowToPlay />)],
  [o => o == 'terms', o => (<Terms />)],
  [o => o == 'faq', o => (<Faq />)],
])(localStorage.currentStage)

class Router extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Switch>
        <Route exact path="/" component={RouteHome} />
        <Route path="/cl" component={Order} />
        <Route path="/how-to-enter" component={HowToPlay} />
        <Route path="/redeemed" render={() => (<CanGo stage="redeemed" Component={Redeemed} />)} />
        <Route path="/application" render={() => (<CanGo stage="application" Component={ApplicationForm} />)} />
        <Route path="/saved" render={() => (<CanGo stage="saved" Component={SaveForLater} />)} />
        <Route path="/claim" render={() => (<CanGo stage="claim" Component={Claim} />)} />
        <Route path="/order" render={() => (<CanGo stage="order" Component={Order} />)} />
        <Route path="/questions" render={() => (<CanGo stage="home" Component={Questions} />)} />
        <Route path="/terms" component={Terms} />
        <Route path="/faq" component={Faq} />
      </Switch>
    )
  }
}

export default Router
