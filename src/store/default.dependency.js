import {HttpClient} from 'components/utilities'
import * as config from 'config'

const client = HttpClient.create({
    baseURL: config.apiUrl
})

export default {
    httpClient: client
}