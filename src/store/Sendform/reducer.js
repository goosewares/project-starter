import { initialState } from './selectors'
import { SEND_FORM_FULFILLED, SEND_FORM_REJECTED, SAVE_LOCATION, REDEEMED, CHECK_REDEEMED, CHECK_REDEEMED_FULFILLED } from './actions'

export default (state = initialState, { type, payload = {} }) => {
  switch (type) {
    case SAVE_LOCATION:
      return {
        ...state,
        selectedVenue: payload,
      }
    case SEND_FORM_FULFILLED:
      if (payload) {
        return {
          ...state,
          form_sent: true,
        }
      }
    case SEND_FORM_REJECTED:
        if (payload) {
            return {
                ...state,
                drink_redeemed: true,
            }
        }
    case CHECK_REDEEMED_FULFILLED:
      if(typeof payload != 'undefined'){
        return{
          ...state,
          drink_redeemed: payload,
          activateCountdown: !payload ? true:'redeemed'
        }
      }
    case REDEEMED:
      if (payload) {
        return {
          ...state,
          drink_redeemed: true,
        }
      }
    default:
      return state
  }
}