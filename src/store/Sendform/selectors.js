import { denormalize } from 'normalizr'
import * as schemas from './schemas'

export const initialState = {
    success: false,
    message: false,
    selectedVenue: false,
    activateCountdown: false,
    drink_redeemed: false
}