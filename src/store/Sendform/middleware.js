import { createLogicMiddleware,createLogic } from 'redux-logic'
import dependencies from './dependency'
import * as logic from './logic'

let logics = [];
Object.keys(logic).map(o=>{
    if(o != 'default'){
        logics.push(createLogic(logic[o]))
    }
})

const middleware = createLogicMiddleware(logics,dependencies)

export default middleware;