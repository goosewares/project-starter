import * as actions from '../actions'
import { actionChannel } from '@redux-saga/core/effects';

module.exports = {
    redeem: {
        type: actions.REDEEM,
        latest: true,
        validate({getState, action},allow, reject){
            if(action.payload){
                allow(action)
            }else{
                reject()
            }
        },
        process({httpClient, getState, action}, dispatch, done){
            httpClient.put(`redeem`,action.payload)
                .then(results=>{
                    return results.data.result
                })
                .then(results=>{
                    console.log(results)
                    if(results.success){
                        dispatch({
                            type: actions.REDEEMED,
                            payload: true
                        })}
                    }
                )
                .catch(err=>{
                    console.log(err);
                    dispatch({
                        type: actions.REDEEM_REJECTED,
                        payload: err,
                        error: true
                    })
                })
                .then(()=>done())
        }
    },
    checkRedeemed: {
        type: actions.CHECK_REDEEMED,
        latest: true,
        validate({getState, action},allow, reject){
            if(action.payload){
                allow(action)
            }else{
                reject()
            }
        },
        process({httpClient, getState, action}, dispatch, done){
            httpClient.post(`check`,action.payload)
                .then(results=>{
                    return results.data.result
                })
                .then(results=>{
                    if(typeof results.redeemed != 'undefined'){
                        dispatch({
                            type: actions.CHECK_REDEEMED_FULFILLED,
                            payload: results.redeemed
                        })}
                    }
                )
                .catch(err=>{
                    dispatch({
                        type: actions.CHECK_REDEEMED_REJECTED,
                        payload: err,
                        error: true
                    })
                })
                .then(()=>done())
        }
    },
    sendform: {
        type: actions.SEND_FORM,
        latest: true,
        validate({getState, action},allow, reject){
            if(action.payload){
                allow(action)
            }else{
                reject()
            }
        },
        process({httpClient, getState, action}, dispatch, done){
            httpClient.put(`insert`,action.payload)
                .then(results=>{
                    return results.data.result
                })
                .then(results=>{
                        if(results.success){
                            localStorage.currentID = action.payload.phone;
                            dispatch({
                                type: actions.SEND_FORM_FULFILLED,
                                payload: true
                            })
                        }
                        if(results.success === false){
                            if(results.message == 'Redeemed') {
                                localStorage.currentID = action.payload.phone;
                                dispatch({
                                    type: actions.SEND_FORM_REJECTED,
                                    payload: true
                                })
                            }
                        }
                    }
                )
                .catch(err=>{
                    console.log(err);
                    dispatch({
                        type: actions.SEND_FORM_REJECTED,
                        payload: err,
                        error: true
                    })
                })
                .then(()=>done())
        }
    }
}