export const SEND_FORM = "SEND_FORM"
export const SEND_FORM_FULFILLED = "SEND_FORM_FULFILLED"
export const SEND_FORM_REJECTED = "SEND_FORM_REJECTED"
export const SAVE_LOCATION = "SAVE_LOCATION"
export const REDEEM = "REDEEM"
export const REDEEMED = "REDEEMED"
export const CHECK_REDEEMED = "CHECK_REDEEMED"
export const CHECK_REDEEMED_FULFILLED = "CHECK_REDEEMED_FULLFILLED"
export const CHECK_REDEEMED_REJECTED = "CHECK_REDEEMED_REJECTED"

export const sendForm = data => {
    return ({type: SEND_FORM, payload: data })
}
export const saveLocation = data => ({type: SAVE_LOCATION, payload: data})
export const redeem = data => ({type: REDEEM, payload: data})
export const check_redeemed = data => ({type: CHECK_REDEEMED, payload: data})