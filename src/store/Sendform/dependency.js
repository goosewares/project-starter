import defaultDeps from '../default.dependency'
const merge = require('lodash/merge')

const dependencies = {

};

export default merge(defaultDeps, dependencies)