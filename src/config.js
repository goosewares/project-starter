const merge = require('lodash/merge')

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    isDev: process.env.NODE_ENV !== 'production',
    basename: process.env.PUBLIC_PATH,
    host: process.env.HOST || 'localhost',
    port: process.env.PORT || 3000,
    isBrowser: typeof window !== 'undefined',
    isServer: typeof window === 'undefined',
    'recaptcha': '6LfQKY8UAAAAAJEDraesZGt0vuYfS0t_asP7Eeub',
    //apiUrl: 'https://stoutshout-backend-stg.vmlaustralia.com/',
    apiUrl: 'https://stoutshout-backend.vmlaustralia.com/',
  },
  test: {},
  development: {
    apiUrl: `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 3000}/api`,
  },
  production: {
    host: process.env.HOST || 'localhost',
    port: process.env.PORT || 8080,
  },
}

module.exports = merge(config.all, config[config.all.env])
